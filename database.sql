CREATE DATABASE clickquest;
USE clickquest;

CREATE TABLE players (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  password_hash VARCHAR(255) NOT NULL,
  gold INT NOT NULL DEFAULT 0,
  is_busy BOOLEAN NOT NULL DEFAULT FALSE,
  xp INT NOT NULL DEFAULT 0,
  level INT NOT NULL DEFAULT 0,
  task_start_time DATETIME DEFAULT NULL,
  current_task_id INT DEFAULT NULL
);

CREATE TABLE tasks (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  time_seconds INT NOT NULL,
  gold INT NOT NULL,
  minimum_level INT NOT NULL
);
